namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailModels",
                c => new
                    {
                        To = c.String(nullable: false, maxLength: 128),
                        Subject = c.String(),
                        Body = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.To);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EmailModels");
        }
    }
}
