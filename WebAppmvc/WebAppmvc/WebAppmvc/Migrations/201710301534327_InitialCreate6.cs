namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate6 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.EmailModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmailModels",
                c => new
                    {
                        To = c.String(nullable: false, maxLength: 128),
                        Subject = c.String(),
                        Body = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.To);
            
        }
    }
}
