namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialCrea : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoginCustomers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            //DropTable("dbo.CustomerLogins");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CustomerLogins",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            DropTable("dbo.LoginCustomers");
        }
    }
}
