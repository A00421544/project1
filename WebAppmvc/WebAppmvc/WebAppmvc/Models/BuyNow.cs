﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public partial class BuyNow
    {
        [Key]
        public int CreditCardNumber { get; set; }
        public String CreditType { get; set; }
        public String NameOnCreditCard { get; set; }
        public DateTime ExpiryDate { get; set; }


    }
}